<%@ page session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>JSP Page</title>
</head>
<body>

<div class="container">
    <h1>Car List</h1>
    <%--<div class="row">
        <div class="col-md-6">
            <form action="<c:url value="/searchCar" />" method="get">
                <input class="form-control input-lg" type="text" placeholder="Model">
                <input type="submit" name="action" value="save" />
            </form>
        </div>
        <div class="col-md-6"><input class="form-control input-lg" type="text" placeholder="Price"></div>
    </div>--%>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Model</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${carList}" var="car">
            <tr><td>${car.model}</td><td>${car.price}</td></tr>
        </c:forEach>
        </tbody>
    </table>
</div>



</body>
</html>