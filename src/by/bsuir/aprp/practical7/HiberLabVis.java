package by.bsuir.aprp.practical7;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;

import java.awt.*;
import java.awt.event.*;

import java.util.List;

public class HiberLabVis extends Frame implements ActionListener {
    StudentDAO dao = new StudentDAO();
    static SessionFactory sessionFactory;
    private static Connection connection = null;
    static boolean dbConnected = false;
    Button exitBtn = new Button("Выход");
    Button connectBtn = new Button("Соединить");
    Button searchBtn = new Button("Поиск");
    Button addBtn = new Button("Добавить");
    Button removeBtn = new Button("Удалить");

    Label nameLbl = new Label("Имя");
    Label ageLbl = new Label("Возраст");
    Label statusLbl = new Label("Статус");

    private static TextField statusInput = new TextField();
    private static TextField nameInput = new TextField();
    private static TextField ageInput = new TextField();
    private TextField searchInput = new TextField();

    Student activeStudent = null;


    public HiberLabVis() {
        super("Практическое занятие №7");
        setLayout(null);
        setSize(350, 450);

        add(exitBtn);
        exitBtn.setBounds(120, 250, 100, 20);
        exitBtn.addActionListener(this);

        add(connectBtn);
        connectBtn.setBounds(120, 280, 100, 20);
        connectBtn.addActionListener(this);

        add(nameLbl);
        nameLbl.setBounds(30, 30, 80, 20);
        add(ageLbl);
        ageLbl.setBounds(30, 60, 80, 20);
        add(nameInput);
        nameInput.setBounds(120, 30, 100, 20);
        add(ageInput);
        ageInput.setBounds(120, 60, 100, 20);
        add(statusLbl);
        statusLbl.setBounds(30, 420, 100, 20);

        add(statusInput);
        statusInput.setBounds(120, 420, 140, 20);
        statusInput.setText("Disconnected");

        searchInput.setBounds(120, 170, 100, 20);
        add(searchInput);

        searchBtn.setBounds(120, 200, 100, 20);
        searchBtn.addActionListener(this);
        add(searchBtn);

        addBtn.setBounds(120, 90, 100, 20);
        addBtn.addActionListener(this);
        add(addBtn);

        removeBtn.setBounds(120, 120, 100, 20);
        removeBtn.addActionListener(this);
        add(removeBtn);

        this.show();
        this.setLocationRelativeTo(null);
    }

    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == exitBtn) {
            /*Session session = sessionFactory.openSession();
            if (session != null) {
                session.flush();
                session.close();
            }*/
            System.exit(0);
        } else if (ae.getSource() == connectBtn)
            connectDb();
        else if (ae.getSource() == searchBtn)
            searchStudent();
        else if (ae.getSource() == addBtn) {
            dao.addStudent(new Student(nameInput.getText(), Integer.parseInt(ageInput.getText())));
            statusInput.setText("Student added");
        }
        else if (ae.getSource() == removeBtn)
            removeStudent();
    }

    private void connectDb() {
        String dbURL = "jdbc:derby://localhost:3306/university;user=root;";

        Session session;

        if (!dbConnected) {
            statusInput.setText("Строим Factory");
            sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
            statusInput.setText("Session Factory построена");
            statusInput.setText("Соединяемся с базой");
            try {
                Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
                connection = DriverManager.getConnection(dbURL);
            } catch (Exception except) {
                statusInput.setText("Ошибка соединения:" + except.getMessage());
            }

            statusInput.setText("Connected :)");
            dbConnected = true;

            activeStudent = dao.getFirst();
            nameInput.setText(activeStudent.getName());
            ageInput.setText(Integer.toString(activeStudent.getAge()));
        } else {
            statusInput.setText("База уже подсоединена");
        }
    }

    private void searchStudent() {
        List<Student> students = dao.searchStudent(searchInput.getText());

        if (students.size() > 0) {
            activeStudent = students.get(0);
            nameInput.setText(activeStudent.getName());
            ageInput.setText(Integer.toString(activeStudent.getAge()));
        } else
            statusInput.setText("Not found");
    }

    private void removeStudent() {
        if (activeStudent != null) {
            dao.removeStudent(activeStudent);
            activeStudent = null;
            nameInput.setText("");
            ageInput.setText("");
            statusInput.setText("Student removed");
        }
    }

    static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static void main(String[] args) {
        new HiberLabVis();
    }
}