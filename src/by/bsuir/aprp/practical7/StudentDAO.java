package by.bsuir.aprp.practical7;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

class StudentDAO {
    Student getFirst() {
        Session session = HiberLabVis.getSessionFactory().openSession();

        return (Student) session.createQuery("from Student").list().get(0);
    }

    List<Student> searchStudent(String name) {
        Session session = HiberLabVis.getSessionFactory().openSession();

        return session.createQuery("from Student where name='" + name + "'").list();
    }

    void addStudent(Student student) {
        Transaction transaction = null;
        Session session = HiberLabVis.getSessionFactory().openSession();

        try {
            transaction = session.beginTransaction();
            session.save(student);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    void removeStudent(Student student) {
        Transaction transaction = null;
        Session session = HiberLabVis.getSessionFactory().openSession();

        try {
            transaction = session.beginTransaction();
            session.remove(student);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
