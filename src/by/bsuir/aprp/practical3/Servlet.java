package by.bsuir.aprp.practical3;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.*;
import javax.servlet.http.*;

public class Servlet extends HttpServlet {

    private static final Map<String, String> RU_ENG_DICT = new HashMap<String, String>() {{
        put("кот", "cat");
        put("собака", "dog");
        put("привет", "hello");
    }};

    private static final Map<String, String> ENG_RU_DICT = new HashMap<String, String>() {{
        put("cat", "кот");
        put("dog", "собака");
        put("hello", "привет");
    }};

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String language = request.getParameter("lang");
        String word, translation = "";

        if ("ru".equals(language)) {
            word = request.getParameter("russian");
            translation = RU_ENG_DICT.get(word);
        } else if ("en".equals(language)) {
            word = request.getParameter("translation");
            translation = ENG_RU_DICT.get(word);
        }

        writer.println(translation);
    }
}


