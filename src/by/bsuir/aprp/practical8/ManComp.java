package by.bsuir.aprp.practical8;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class ManComp {
    private String input;
    private String output;

    public ManComp() {
    }

    public String getAnswer(String x) {
        return "Hello from JSF " + x;
    }

    public void submit() {
        output = "You Are Welcome!!! " + input;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}
