package by.bsuir.aprp.practical1;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Arrays;

public class Practical1 extends Frame implements ActionListener {
    private Button exitBtn = new Button("Exit");
    private Button searchBtn = new Button("Search");
    private TextArea textArea = new TextArea();

    private int maxOccurence = 0;
    private int maxOccurenceIndex = 0;

    private Practical1() {
        super("Практическое занятие №1");
        setLayout(null);
        setBackground(new Color(239, 240, 241));
        setSize(260, 190);

        add(exitBtn);
        add(searchBtn);
        add(textArea);

        textArea.setBounds(20, 40, 220, 100);

        searchBtn.setBounds(140, 155, 100, 20);
        searchBtn.addActionListener(this);

        exitBtn.setBounds(20, 155, 100, 20);
        exitBtn.addActionListener(this);

        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (exitBtn == actionEvent.getSource()) {
            System.exit(0);
        } else if (searchBtn == actionEvent.getSource()) {
            String[] keywords = textArea.getText().split(",");
            for (String keyword : keywords) {
                keyword = keyword.trim();
                System.out.println(keyword);
            }

            File sourceFolder = new File("/Users/egor/University/AiPRP/Practical/src/by/bsuir/aprp/practical1/sources");

            ArrayList<File> files = new ArrayList<>(Arrays.asList(sourceFolder.listFiles()));
            textArea.setText("");

//            for (File file : files) {
            for (int i = 0; i < files.size(); i++) {
                File file = files.get(i);
                int matches = test_url(file, keywords);
                textArea.append(file.getName() + " : " + matches + "\n");

                if (matches > maxOccurence) {
                    maxOccurence = matches;
                    maxOccurenceIndex = i;
                }
            }

            textArea.append("Max matches is " + maxOccurence + " from #" + maxOccurenceIndex + 1 + "\n");

            try {
                Desktop.getDesktop().browse(files.get(maxOccurenceIndex).toURI());
            } catch (IOException ioException) {
                System.out.println("error " + ioException.getMessage());
            }
        }
    }

    private static int test_url(File file, String[] keywords) {
        int result = 0;
        URLConnection connection;

        try {
            connection = new URL("file://" + file.getAbsolutePath()).openConnection();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));

            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();

            for (String keyword : keywords) {
                if (response.toString().contains(keyword))
                    result++;
            }

        } catch (MalformedInputException malformedInputException) {
            System.out.println("error " + malformedInputException.getMessage());
            return -1;
        } catch (IOException ioException) {
            System.out.println("error " + ioException.getMessage());
            return -1;
        } catch (Exception e) {
            System.out.println("error " + e.getMessage());
            return -1;
        }
        return result;
    }

    public static void main(String[] args) {
        new Practical1();
    }
}
