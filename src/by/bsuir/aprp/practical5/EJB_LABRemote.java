package by.bsuir.aprp.practical5;

import javax.ejb.Remote;

@Remote
public interface EJB_LABRemote {
    String getStudentInfo(String name);
}
