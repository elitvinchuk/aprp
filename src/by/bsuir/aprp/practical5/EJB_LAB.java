package by.bsuir.aprp.practical5;

import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

@Stateless
public class EJB_LAB implements EJB_LABRemote, Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String department;
    private String group;


    private static final ArrayList<Student> students
            = new ArrayList<>(Arrays.asList(
            new Student("ivanov", "Marketing", "10"),
            new Student("petrov", "Marketing", "10"),
            new Student("sidorov", "Professor", "12"),
            new Student("mishin", "Smith", "12"),
            new Student("vasin", "Programmer", "14")
    ));

    @Override
    public String getStudentInfo(String name) {
        String rez = "???";

        for (Student student : students) {
            System.out.println(student.getName());
            if (name.equals(student.getName())) {
                rez = student.getDepartment() + "  " + student.getGroup();
                break;
            }
        }

        return rez;
    }

    public String addStudent() {
        Student std =
                new Student(name,department,group);
        students.add(std);
        return null;
    }

    public String deleteStud(Student em) {
        students.remove(em);
        return null;
    }


    public String getFio() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

}
