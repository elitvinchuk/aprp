package by.bsuir.aprp.practical5;

import javax.ejb.EJB;

public class Main {

    @EJB
    private static EJB_LABRemote service;

    public static void masin(String[] args) {
        System.out.println(service);
        System.out.println(service.getStudentInfo("ivanov"));
    }

    public static void setService(EJB_LABRemote service) {
        Main.service = service;
    }
}
