package by.bsuir.aprp.practical5;

class Student {
    private String name;
    private String department;
    private String group;

    Student(String name, String department, String group) {
        this.name = name;
        this.department = department;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
