package by.bsuir.aprp.practical4;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;

public class WebServiceClient {

    private WebServiceClient() {
        Frame mainFrame = new Frame("Практическое заняте №4");

        mainFrame.setSize(660, 210);
        mainFrame.setLayout(new FlowLayout());
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        TextArea contentArea = new TextArea();

        //region Fill with data
        FileUpload service = new FileUpload();
        ArrayList<File> books = service.getCatalogue();

        List booksList = new List(books.size(), false);
        booksList.addItemListener( e ->
            contentArea.setText(new String(
                    service.getFile(
                            books.get(booksList.getSelectedIndex())
                    )
            ))
        );
        /*booksList.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                contentArea.setText(new String(
                        service.getFile(
                                books.get(booksList.getSelectedIndex())
                        )
                ));
            }
        });*/

        for (File book : books) {
            booksList.add(book.getName());
        }

        mainFrame.add(booksList);
        //endregion

        contentArea.setText(new String(service.getFile(books.get(0))));
        mainFrame.add(contentArea);

        mainFrame.setVisible(true);
    }

    public static void main(String[] args) {
        new WebServiceClient();
    }
}
