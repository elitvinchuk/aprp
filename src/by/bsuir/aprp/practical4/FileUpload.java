package by.bsuir.aprp.practical4;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.awt.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

@WebService(serviceName = "FileUpload")
public class FileUpload {
    File basePath = new File("/Users/egor/University/AiPRP/Practical/src/by/bsuir/aprp/practical4/books");

    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String text) {
        return "Hello " + text + "!";
    }

    @WebMethod(operationName = "getCatalogue")
    public ArrayList<File> getCatalogue() {
        return new ArrayList<>(Arrays.asList(basePath.listFiles()));
    }

    @WebMethod(operationName = "getFile")
    public byte[] getFile(@WebParam(name = "filePath") File filePath) {
        byte[] buffer = null;

        try {
            Path path = Paths.get(filePath.getPath());
            buffer = Files.readAllBytes(path);
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        return buffer;
    }
}
