package by.bsuir.aprp.practical2;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Practical2 extends Frame implements ActionListener {
    private Button client1StartBtn = new Button("Клиент #1");
    private Button client2StartBtn = new Button("Клиент #2");
    static TextArea msgBox = new TextArea();

    private Practical2() {
        super("Практическое занятие №2");
        setLayout(null);
        setSize(360, 280);

        client1StartBtn.setBounds(20, 40, 150, 50);
        client1StartBtn.addActionListener(this);

        client2StartBtn.setBounds(190, 40, 150, 50);
        client2StartBtn.addActionListener(this);

        msgBox.setBounds(20, 110, 320, 150);

        add(client1StartBtn);
        add(client2StartBtn);
        add(msgBox);

        this.setVisible(true);
        msgBox.append("Приложение стартовало.\n");
    }

    public static void main(String[] args) {
        new Practical2();
        new Server().start();
    }

    public boolean handleEvent(Event event) {
        if (event.id == Event.WINDOW_DESTROY) {
            System.exit(0);
        }

        return super.handleEvent(event);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (client1StartBtn == actionEvent.getSource()) {
            new Client1().start();
        } else if(client2StartBtn == actionEvent.getSource()) {
            new Client2().start();
        }
    }
}
