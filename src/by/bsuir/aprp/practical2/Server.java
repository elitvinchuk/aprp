package by.bsuir.aprp.practical2;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {
    private static int amount = 200;

    public void run() {
        Socket socket;
        ServerSocket server;
        Practical2.msgBox.append("[Server] Сервер запущен.\n");
        Practical2.msgBox.append("[Server] Изначальный баланс: " + amount + ".\n");

        try {
            server = new ServerSocket(3001);

            while (true) {
                socket = server.accept();

                PrintStream stream = new PrintStream(socket.getOutputStream());
                int chunk = (int) (Math.random() * 1000);

                if (Math.random() > 0.5)
                    amount -= chunk;
                else
                    amount += chunk;

                stream.println("[Server] Transaction: " + amount + " (" + chunk + ")");
                stream.flush();

                socket.close();
            }
        } catch (IOException e) {
            System.out.println("Connection error: " + e);
        }
    }
}
