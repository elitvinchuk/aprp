package by.bsuir.aprp.practical2;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class Client1 extends Thread {
    private DataInputStream inputStream;

    public Client1() {
        Socket socket;

        try {
            socket = new Socket("127.0.0.1", 3001);
            inputStream = new DataInputStream(socket.getInputStream());

            Practical2.msgBox.append("Клиент #1 отправил запрос.\n");
        } catch (IOException e) {
            System.out.println("Socket error: " + e);
        }
    }

    public void run() {
        while (true) {
            {
                try {
                    sleep(100);

                    String message = inputStream.readLine();

                    if (message == null)
                        break;

                    Practical2.msgBox.append(message + "\n");
                } catch (Exception e) {
                    System.out.println("Error: " + e);
                }
            }
        }
    }
}
