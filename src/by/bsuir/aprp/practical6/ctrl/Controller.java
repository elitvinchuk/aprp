package by.bsuir.aprp.practical6.ctrl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Controller implements org.springframework.web.servlet.mvc.Controller {
    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        CarManager carManager = new CarManager();

        ModelAndView modelAndView = new ModelAndView("main.jsp");
        modelAndView.addObject("carList", carManager.getCarList());

        return modelAndView;
    }

    @RequestMapping(value = "/searchCar", method = RequestMethod.GET)
    public String searchCar(@RequestParam String model) {
        return "MODEL";
    }

}
